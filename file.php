<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>
        GET
    </title>
</head>
<body>
<div class="container">
    <h2>Upload Files</h2>
    <form class="form-horizontal" role="form" action="fileone.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label class="control-label col-sm-2" for="text">Name</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" id="text" placeholder="Enter Name" name="name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="upload">Upload File</label>
            <div class="col-sm-5">
                <input type="file" class="form-control" id="file" placeholder="Choose File" name="fileToUpload">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default" value="submit">Submit</button>
            </div>
        </div>
    </form>
</div>

</body>

</html>

